import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import configuration from './config/configuration';
import { AppService } from './app.service';

describe('AppService', () => {
  let appService: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          load: [configuration],
          validationSchema: Joi.object({
            foo: Joi.string(),
            database: Joi.object({
              port: Joi.number(),
            }),
          }), 
        }),
      ],
      providers: [
        AppService,
      ],
    }).compile();

    appService = app.get<AppService>(AppService);
  });

  describe('#getFoo()', () => {

    it('should return "BAR', () => {
      const whenDone = appService.getFoo();

      expect(whenDone).toBe('bar');
    });

  });

  describe('#getNestedPropFromConfig()', () => {

    it('should return 123', () => {
      const nestedProp = appService.getNestedPropFromConfig();

      expect(nestedProp).toBe(123);
    });

  });

  describe('#trasverseNestedPropFromConfig()', () => {

    it('should return 123', () => {
      const nestedProp = appService.trasverseNestedPropFromConfig();

      expect(nestedProp).toBe(123);
    });

  });

});
