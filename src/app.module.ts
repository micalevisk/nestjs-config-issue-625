import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import configuration from './config/configuration';

@Module({
  imports: [ConfigModule.forRoot({
    load: [configuration],
    validationSchema: Joi.object({
      foo: Joi.string(),
      database: Joi.object({
        port: Joi.number(),
      }),
    }),
  })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
