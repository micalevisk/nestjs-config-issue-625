import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { AppModule } from './app.module';
import { IConfig } from './config/configuration';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // const configService = app.get< ConfigService<IConfig, true> >(ConfigService);
  const configService = app.get(ConfigService);
  // const configService = app.get< ConfigService >(ConfigService);

  console.log(
    //
    // configService.get('database.port', 123, { infer: true }),
    configService.get('database.port', 123),
  );
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
