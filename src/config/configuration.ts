export interface IDatabaseConfig {
  port: number
}

export interface IConfig {
  foo: string
  database: IDatabaseConfig
}

export default () => ({
  foo: process.env.FOO,

  database: {
    port: 123,
  },
});

